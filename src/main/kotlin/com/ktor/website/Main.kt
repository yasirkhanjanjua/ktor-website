package com.ktor.website

import freemarker.cache.ClassTemplateLoader
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing

import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import java.lang.Exception

data class IndexData(val items: List<Int>)

fun Application.module() {

    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(this::class.java.classLoader, "templates")
    }

    routing {

        static("/static") {
            resources("static")
        }

        get("/html-freemarker") {
            try {
                call.respond(FreeMarkerContent("index.ftl", mapOf("data" to IndexData(listOf(1, 2, 3))), ""))
            } catch (e: Exception) {
                println("Something went wrong $e")
            }
        }

        route("/login") {
            get {
                call.respond(FreeMarkerContent("login.ftl", null))
            }

            post {
                val post = call.receiveParameters()
                println("Form parameters $post")
                if (isValidInput(post["username"]) && post["username"] == post["password"]) {
                    call.respondText("OK")
                } else {
                    call.respond(FreeMarkerContent("login.ftl", mapOf("error" to "Invalid login")))
                }
            }
        }

        route("/webview") {
            get {
                call.respond(FreeMarkerContent("webview.ftl", null))
            }
        }
    }
}

private fun isValidInput(userName: String?) = userName.orEmpty().isNotBlank()

fun main(args: Array<String>) {
    embeddedServer(Netty, 8080, watchPaths = listOf("MainKt"), module = Application::module).start()
}