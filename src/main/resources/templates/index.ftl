<#-- @ftlvariable name="data" type="com.ktor.website.IndexData" -->
<html>
 <head>
        <link rel="stylesheet" href="/static/styles.css">
 </head>
	<body>
		<ul>
		<#list data.items as item>
			<li>${item}</li>
		</#list>
		</ul>
	</body>
</html>
